package com.company;

import java.util.concurrent.Semaphore;

public class MyThread extends Thread
{
	private static Semaphore semaphore = new Semaphore(3);
	private String name;
	
	public MyThread(String name)
	{
		this.name = name;
	}
	
	public static Semaphore getSemaphore()
	{
		return semaphore;
	}
	
	@Override
	public void run()
	{
		try
		{
			System.out.println(name + " : locking...");
			System.out.println(name + " : available permits now: " + semaphore.availablePermits());
			semaphore.acquire();
			System.out.println(name + " : got the permit");
			try
			{
				for (int i = 1; i <= 5; i++)
				{
					System.out.println(name + " : is performing operation " + i + ", available Semaphore permits : " + semaphore.availablePermits());
					Thread.sleep(1000);
				}
			}
			finally
			{
				System.out.println(name + " : releasing lock...");
				semaphore.release();
				System.out.println(name + " : available Semaphore permits now: " + semaphore.availablePermits());
			}
		}
		catch (InterruptedException e)
		{
			e.printStackTrace();
		}
	}
}
