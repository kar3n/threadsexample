package com.company;

public class Main
{
	
	public static void main(String[] args)
	{
		MyThread t1 = new MyThread("A");
		t1.start();
		
		MyThread t2 = new MyThread("B");
		t2.start();
		
		MyThread t3 = new MyThread("C");
		t3.start();
		
		MyThread t4 = new MyThread("D");
		t4.start();
		
		MyThread t5 = new MyThread("E");
		t5.start();
		
		MyThread t6 = new MyThread("F");
		t6.start();
	}
}
